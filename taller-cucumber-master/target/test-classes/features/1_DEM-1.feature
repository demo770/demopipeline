@DEM-2
Feature: test google

	@TEST_DEM-1
	Scenario: Verify open google
		Given I launch Chrome browser
		When I open Google Homepage
		Then I verify that the page displays search text box
		And the page displays Google Search button
		And the page displays Im Feeling Lucky button
		And the browser close
